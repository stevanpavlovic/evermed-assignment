<?php

namespace StevanPavlovic\EvermedAssignment\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

final class LatLng extends Constraint
{
    public $message = 'The values for latitude and longitude ("%lat%" and "%lng%") are not valid.';
}