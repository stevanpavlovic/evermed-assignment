<?php

namespace StevanPavlovic\EvermedAssignment\Validator\Constraints;

use InvalidArgumentException;
use StevanPavlovic\EvermedAssignment\RawCoordinates;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

final class LatLngValidator extends ConstraintValidator
{
    public function validate($coordinates, Constraint $constraint): void
    {
        if (!($coordinates instanceof RawCoordinates)) {
            throw new InvalidArgumentException('Provided argument is not of type ' . RawCoordinates::class . '.');
        }

        if (!preg_match('/^\-?([0-8]?[0-9](\.[0-9]+)?|90)$/', $coordinates->getLat(), $matches)
            || !preg_match('/^\-?([0-9]?[0-9](\.[0-9]+)?|1[0-7][0-9](\.[0-9]+)?|180)$/', $coordinates->getLng(), $matches)
        ) {
            $this->context->addViolation($constraint->message, [
                'lat' => $coordinates->getLat(),
                'lng' => $coordinates->getLng(),
            ]);
        }
    }
}