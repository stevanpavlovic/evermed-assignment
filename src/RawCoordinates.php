<?php

namespace StevanPavlovic\EvermedAssignment;

interface RawCoordinates
{
    public function getLat(): string;
    
    public function getLng(): string;
}